package pascal;

public class pascal {
	
	public static int recuperer_indice(int n, int p) {
		return (n*(n+1)/2) + p;
	}
	
	public static int [] recuperer_indice_inverse(int i){
		int [] indices = new int [2];
		indices[0] = (-1+(int)Math.sqrt(1 - 4 * 1 * -(i*2)))/2;
		indices[1] = i - (indices[0]*(indices[0]+1)/2);
		return indices;
	}
	
	public static int recuperer_valeur_maximale(int [] t) {
		int max = 0;
		for(int i = 0; i<t.length; i++) {
			if(t[i]>max) {
				max = t[i];
			}
		}
		return max;
	}
	
	public static int [] generer_triangle(int lignes) {
		int [] t = new int [(lignes+1)*(lignes+2)/2];
		t[0] = 1;
		for(int i = lignes;i>=0; i--) {
			generer_coefficient(t,lignes,i);
		}	
		return t;
	}
	
	public static void afficher_triangle(int [] t) {
		String espaces;
		
		int max = recuperer_valeur_maximale(t);
		
		int longueur_max_coef = Integer.toString(max).length();		
		int longueur_max_n = Integer.toString(recuperer_indice_inverse(t.length-1)[0]).length();
		
		for(int j = 0; j<t.length; j++) {
			int [] np = recuperer_indice_inverse(j);
			
			if(np[1] == 0) {
				System.out.print(" ".repeat(longueur_max_n - Integer.toString(np[0]).length()));
				System.out.print(np[0] + " : ");
			}
			
			espaces = " ";
			for(int k = longueur_max_coef; k>0; k--) {
				espaces += (t[j] / Math.pow(10, k)) >= 1 ? "":" ";  
			}
			System.out.print(t[j] + espaces);
			
			if(np[0] == np[1]) System.out.println();
		}
	}
	
	public static int generer_coefficient(int [] t, int n, int p) {
		int i = recuperer_indice(n,p);
		if(t[i] == 0) {
			if(p == 0) {
				t[i] = 1;
			} else if(p == n) {
				t[i] = 1;
			} else {
				t[i] = generer_coefficient(t,n-1,p-1)+generer_coefficient(t,n-1, p);
			}
		}
		return t[i];
	}
	
	public static void main(String[] args) {
		int nb_lignes = 25;
		
		int [] triangle_de_pascal = generer_triangle(nb_lignes);
		afficher_triangle(triangle_de_pascal);
	}

}
